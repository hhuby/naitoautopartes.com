<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'naitoaut_wp');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'naitoaut_root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', '123456');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'HZWz`]Iezxjg?<`zLHv(1`+ &}EDVV^O)#ykp(Ral%%_uwzo&s||qL@ne|dQHg]H'); // Cambia esto por tu frase aleatoria.
define('SECURE_AUTH_KEY', '+5!pXKc<D$xh6]T0m}`~I)U<;ZUM|o*)|M|wR I1X )?oH`z TV{Rt @| .$S6ZM'); // Cambia esto por tu frase aleatoria.
define('LOGGED_IN_KEY', ' g=f@gTErK(IQhFikcp-DQu`IWf1kp@}V!Xo[f_-oEa`|GUn)2_^a/r<D,]rk^Jd'); // Cambia esto por tu frase aleatoria.
define('NONCE_KEY', '^W|.6vU*1KpEE-Y-hX$S?0<[]:WN9]f(wN@%wT:S`l|L!YUuwiz87j>P?)/tFCLl'); // Cambia esto por tu frase aleatoria.
define('AUTH_SALT', '_q+ 8Eg3>dbFk`%5^-c7m2I<;6mTdO(yO+_TjZe 4|49,b??K/SrUqq)ELv?-ft.'); // Cambia esto por tu frase aleatoria.
define('SECURE_AUTH_SALT', '/a5znDz$,>qiY[6i9A+?2hRBcW}j.jTZGhV|~iPyp^h&5M?GmA7=``DM/bDM$8qT'); // Cambia esto por tu frase aleatoria.
define('LOGGED_IN_SALT', '4Q91!sEi94*x/=@^+.>5zj3&&;RHt:)ix<a6/+T<U-F7U>l_>X7;f,xt=Ly=zxJ-'); // Cambia esto por tu frase aleatoria.
define('NONCE_SALT', '!_@bte1Cs*Y.1,/hi}83TQn|j!~/-O@@4yG4(+_=|qJpC60>s2eRRr2U{CwK`P=p'); // Cambia esto por tu frase aleatoria.

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wp_';

/**
 * Idioma de WordPress.
 *
 * Cambia lo siguiente para tener WordPress en tu idioma. El correspondiente archivo MO
 * del lenguaje elegido debe encontrarse en wp-content/languages.
 * Por ejemplo, instala ca_ES.mo copiándolo a wp-content/languages y define WPLANG como 'ca_ES'
 * para traducir WordPress al catalán.
 */
define('WPLANG', 'es_ES');

/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

